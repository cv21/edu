package util

import (
	"reflect"
)

// Returns first element from given slice or array.
func First(data interface{}) interface{} {
	val := reflect.ValueOf(data)

	if (val.Kind() != reflect.Slice && val.Kind() != reflect.Array) || (val.Kind() == reflect.Slice && val.IsNil()) {
		return nil
	}

	if val.Len() > 0 {
		return val.Index(0).Interface()
	}

	return nil
}
