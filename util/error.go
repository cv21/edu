package util

import "errors"

func Err(val ...interface{}) (out error) {
	for _, v := range val {
		var errVal error
		switch v.(type) {
		case string:
			errVal = errors.New(v.(string))
		case error:
			errVal = v.(error)
		}

		if out == nil {
			out = errVal
		}
	}

	return
}
