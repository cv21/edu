NAME=edu

tests:
	go test ./test/... -v

run:
	go run cmd/main.go

build-all: build
	rm -rf ./public/**
	cd ../edu-frontend && npm run build && cp -r ./build/* ../edu/public
	git add ./public

gcloud-build:
	docker build . -t gcr.io/heeb-243707/api:latest

gcloud-push:
	docker push gcr.io/heeb-243707/api:latest