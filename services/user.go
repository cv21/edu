package services

import (
	"context"
	"errors"

	"time"

	"bitbucket.org/cv21/edu/repository"
	"bitbucket.org/cv21/edu/util"
	"github.com/dgrijalva/jwt-go"
	"github.com/satori/go.uuid"
	"golang.org/x/crypto/bcrypt"
)

const (
	TokenLifetime = 60 * 60 * 24

	TokenKeyExp      = "exp"
	TokenKeyID       = "jti"
	TokenKeyIssuedAt = "iat"
	TokenKeyUserID   = "uid"
)

var (
	ErrEmailExist          = errors.New("email already registered")
	ErrCouldNotDecodeToken = errors.New("could not decode token")
)

type UserFindParams struct {
	IDs []string
}

type User interface {
	Login(ctx context.Context, email, password string) (string, error)
	CheckToken(ctx context.Context, t string) (*repository.User, error)
	Register(ctx context.Context, email, password string) (*repository.User, string, error)
	Find(ctx context.Context, params UserFindParams) ([]*repository.User, error)
}

func NewUser(userRepo repository.UserRepository) User {
	return &user{
		UserRepo: userRepo,
	}
}

type user struct {
	UserRepo repository.UserRepository
}

func (s *user) Login(ctx context.Context, email, password string) (string, error) {
	u, err := s.UserRepo.FindOne("email = ?", email)
	if err != nil {
		return "", err
	}

	err = bcrypt.CompareHashAndPassword([]byte(u.Password), []byte(password))
	if err != nil {
		return "", err
	}

	return s.buildToken(u)
}

func (s *user) CheckToken(ctx context.Context, t string) (*repository.User, error) {
	token, err := jwt.Parse(t, func(token *jwt.Token) (interface{}, error) {
		return []byte(repository.SigningKey), nil
	})
	if err != nil {
		return nil, util.Err(ErrCouldNotDecodeToken, err)
	}

	var uid string
	if claims, ok := token.Claims.(jwt.MapClaims); ok && token.Valid {
		if uid, ok = claims[TokenKeyUserID].(string); ok {
			return s.UserRepo.FindOne("id = ?", uid)
		}
	}

	return nil, ErrCouldNotDecodeToken
}

func (s *user) Register(ctx context.Context, email, password string) (*repository.User, string, error) {
	isExist, err := s.UserRepo.IsEmailExist(email)
	if err != nil {
		return nil, "", err
	}

	if isExist {
		return nil, "", ErrEmailExist
	}

	pwHash, err := bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
	if err != nil {
		return nil, "", err
	}

	// save User
	newUser := &repository.User{
		Email:    email,
		Password: string(pwHash),
	}

	newUser, err = s.UserRepo.CreateUser(newUser)
	if err != nil {
		return nil, "", err
	}

	// and login
	t, err := s.buildToken(newUser)
	if err != nil {
		return nil, "", err
	}

	return newUser, t, nil
}

func (s *user) Find(ctx context.Context, params UserFindParams) ([]*repository.User, error) {
	return s.UserRepo.FindMultiple("id in (?)", params.IDs)
}

func (s *user) buildToken(u *repository.User) (string, error) {
	t, err := jwt.NewWithClaims(jwt.SigningMethodHS256, &jwt.MapClaims{
		TokenKeyExp:      time.Now().Add(TokenLifetime * time.Second).Unix(),
		TokenKeyID:       uuid.NewV4().String(),
		TokenKeyIssuedAt: time.Now().Unix(),
		TokenKeyUserID:   u.ID,
	}).SignedString([]byte(repository.SigningKey))

	if err != nil {
		return "", err
	}
	return t, nil
}
