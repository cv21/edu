package services

import (
	"context"
	"fmt"
	"github.com/go-telegram-bot-api/telegram-bot-api"
	"time"

	"bitbucket.org/cv21/edu/repository"
)

const myChatID = 192979586

type notifier struct {
	tgBot  *tgbotapi.BotAPI
	noteRp repository.NoteRepository
}

type Notifier interface {
	Run() error
	DoRun() error
}

func NewNotifier(noteRp repository.NoteRepository, tgBot *tgbotapi.BotAPI) Notifier {
	return &notifier{
		tgBot:  tgBot,
		noteRp: noteRp,
	}
}

func (n *notifier) DoRun() error {
	notes, err := n.fetchNotes(time.Now())
	if err != nil {
		return err
	}

	for _, note := range notes {
		err = n.sendNotification(note)
		if err != nil {
			return err
		}
	}

	return nil
}

func (n *notifier) Run() error {
	for {
		err := n.DoRun()
		if err != nil {
			return err
		}

		tn := time.Now().AddDate(0, 0, 1)

		time.Sleep(time.Until(time.Date(tn.Year(), tn.Month(), tn.Day(), 7, 0, 0, 0, time.Local)))
	}


	return nil
}

func (n *notifier) fetchNotes(t time.Time) ([]*repository.Note, error) {
	return n.noteRp.List(context.Background(), &repository.NoteListParams{
		CreatedAtDate: buildTimeSeries(t),
	})
}

func (n *notifier) sendNotification(note *repository.Note) error {
	d := time.Until(note.CreatedAt).Round(time.Hour*24)

	msg := tgbotapi.NewMessage(myChatID, fmt.Sprintf("<b>%s</b> \n%s\n\nLearned %d days ago (<a href=\"http://heeb.herokuapp.com/notes/%s\">link</a>)", note.Title, note.Text, int(d/-24/time.Hour), note.ID))
	//msg := tgbotapi.NewMessage(myChatID, fmt.Sprintf(`<b>%s</b> \n%s\n\nLearned %d days ago (<a href="http://heeb.herokuapp.com/notes/%s">link</a>)`, note.Title, note.Text, int(d/-24/time.Hour), note.ID))
	//msg := tgbotapi.NewMessage(myChatID, fmt.Sprintf(`<a href="http://heeb.herokuapp.com/notes/%s">%s</a> <br> %s <br> Learned %d days ago'`, note.ID, note.Title, note.Text, int(d/-24/time.Hour)))
	msg.ParseMode = tgbotapi.ModeHTML

	_, err := n.tgBot.Send(msg)
	return err
}

// Builds interval repitition time series.
func buildTimeSeries(t time.Time) []time.Time {
	ddt := t.AddDate(0, 0, -1)
	tt := []time.Time{time.Date(ddt.Year(), ddt.Month(), ddt.Day(), 0, 0, 0, 0, time.Local)}

	for i := 1; i < 8; i++ {
		dt := tt[i-1].AddDate(0,0, (i+1) * -2)
		tt = append(tt, time.Date(dt.Year(), dt.Month(), dt.Day(), 0, 0, 0, 0, time.Local))
	}

	return tt
}
