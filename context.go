package edu

import (
	"context"
	"errors"

	"reflect"

	"bitbucket.org/cv21/edu/repository"
	"bitbucket.org/cv21/edu/services"
	"bitbucket.org/cv21/edu/util"
	"github.com/jinzhu/gorm"
)

type ContextKey string

type (
	keyTypeDB          string
	keyTypeUser        string
	keyTypeNote        string
	keyTypeCurrentUser string
)

var (
	keyDB          keyTypeDB
	keyUser        keyTypeUser
	keyNote        keyTypeNote
	keyCurrentUser keyTypeCurrentUser
)

var (
	ErrContextValueIsEmpty        = errors.New("context value is empty")
	ErrUnexpectedContextValueType = errors.New("unexpected context value type")
)

func GetDBFromCtx(ctx context.Context) (*gorm.DB, error) {
	cv := ctx.Value(keyDB)
	if cv == nil {
		return nil, ErrContextValueIsEmpty
	}

	v, ok := cv.(*gorm.DB)
	if !ok {
		return nil, util.Err(ErrUnexpectedContextValueType, reflect.TypeOf(keyDB).String())
	}

	return v, nil
}

func SetDBToCtx(ctx context.Context, db *gorm.DB) context.Context {
	return context.WithValue(ctx, keyDB, db)
}

func GetUserFromCtx(ctx context.Context) (services.User, error) {
	cv := ctx.Value(keyUser)
	if cv == nil {
		return nil, ErrContextValueIsEmpty
	}

	v, ok := cv.(services.User)
	if !ok {
		return nil, util.Err(ErrUnexpectedContextValueType, reflect.TypeOf(keyUser).String())
	}

	return v, nil
}

func SetUserToCtx(ctx context.Context, db services.User) context.Context {
	return context.WithValue(ctx, keyUser, db)
}

func GetCurrentUserFromCtx(ctx context.Context) (*repository.User, error) {
	cv := ctx.Value(keyCurrentUser)
	if cv == nil {
		return nil, ErrContextValueIsEmpty
	}

	v, ok := cv.(*repository.User)
	if !ok {
		return nil, util.Err(ErrUnexpectedContextValueType, reflect.TypeOf(keyCurrentUser).String())
	}

	return v, nil
}

func SetCurrentUserToCtx(ctx context.Context, db *repository.User) context.Context {
	return context.WithValue(ctx, keyCurrentUser, db)
}

func GetNoteFromCtx(ctx context.Context) (repository.NoteRepository, error) {
	cv := ctx.Value(keyNote)
	if cv == nil {
		return nil, ErrContextValueIsEmpty
	}

	v, ok := cv.(repository.NoteRepository)
	if !ok {
		return nil, util.Err(ErrUnexpectedContextValueType, reflect.TypeOf(keyNote).String())
	}

	return v, nil
}

func SetNoteToCtx(ctx context.Context, n repository.NoteRepository) context.Context {
	return context.WithValue(ctx, keyNote, n)
}
