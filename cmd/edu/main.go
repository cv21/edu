package main

import (
	"context"
	"flag"
	"fmt"
	"github.com/koron/go-spafs"
	"log"
	"net/http"
	"os"

	"bitbucket.org/cv21/edu"
	"bitbucket.org/cv21/edu/repository"
	"bitbucket.org/cv21/edu/schema"
	"bitbucket.org/cv21/edu/services"
	"github.com/go-telegram-bot-api/telegram-bot-api"
	g "github.com/graphql-go/graphql"
	"github.com/graphql-go/handler"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
)

func init() {
	flag.Parse()
}

func main() {
	logger := log.New(os.Stdout, "app", log.LstdFlags)

	db := initDb()
	defer db.Close()

	db.AutoMigrate(
		&repository.User{},
		&repository.Note{},
	)

	ctx := context.Background()

	userSvc := services.NewUser(repository.NewUserRepository(db))
	ctx = edu.SetDBToCtx(ctx, db)
	ctx = edu.SetUserToCtx(ctx, userSvc)

	noteRepository := repository.NewNoteRepository(db)
	ctx = edu.SetNoteToCtx(ctx, noteRepository)

	gSchema, err := g.NewSchema(schema.Schema)
	if err != nil {
		logger.Fatalf("unable to initialize schema: %v", err)
	}

	h := handler.New(&handler.Config{
		Schema: &gSchema,
		Pretty: true,
	})

	tgBot, err := tgbotapi.NewBotAPI("732603671:AAGLunXcAhS2muLmQ-T5MO587GFhRu42mag")
	if err != nil {
		//logger.Fatal(err)
	}

	ntf := services.NewNotifier(noteRepository, tgBot)
	//go ntf.Run()

	http.Handle("/", spafs.FileServer(http.Dir("./public")))
	http.Handle("/graphql", contextMiddleware(h, ctx, userSvc))
	http.Handle("/notify", http.HandlerFunc(func(resp http.ResponseWriter, req *http.Request) {
		ntf.DoRun()
	}))
	err = http.ListenAndServe(fmt.Sprintf(":%s", os.Getenv("PORT")), nil)
	if err != nil {
		panic(err)
	}
}

func initDb() *gorm.DB {
	db, err := gorm.Open("postgres", os.Getenv("DATABASE_URL"))
	if err != nil {
		panic(err)
	}
	return db
}

func contextMiddleware(h *handler.Handler, ctx context.Context, userSvc services.User) http.Handler {
	return http.HandlerFunc(func(res http.ResponseWriter, req *http.Request) {

		// Setup CORS.
		res.Header().Set("Access-Control-Allow-Origin", "*")
		res.Header().Set("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept")

		token := req.Header.Get("Authorization")

		// Check user token.
		// Set nil *repository.User value for checking if validation succeeded, but user is not defined.
		user, _ := userSvc.CheckToken(ctx, token)
		ctx = edu.SetCurrentUserToCtx(ctx, user)

		h.ContextHandler(ctx, res, req)
	})
}
