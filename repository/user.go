//go:generate moq -out user_mock.go . UserRepository

package repository

import (
	"time"

	"github.com/jinzhu/gorm"
	"github.com/satori/go.uuid"
)

const SigningKey = "paramparampam"

type User struct {
	ID        string `gorm:"type:uuid;primary_key"`
	Email     string
	Password  string
	CreatedAt time.Time
	UpdatedAt time.Time
	DeletedAt *time.Time `sql:"index"`
}

func (u *User) BeforeCreate() error {
	if u.ID == "" {
		u.ID = uuid.NewV4().String()
	}

	return nil
}

type UserRepository interface {
	IsEmailExist(e string) (bool, error)
	CreateUser(user *User) (*User, error)
	FindOne(args ...interface{}) (*User, error)
	FindMultiple(args ...interface{}) ([]*User, error)
}

type userRepository struct {
	db *gorm.DB
}

func NewUserRepository(db *gorm.DB) UserRepository {
	return &userRepository{db}
}

func (r *userRepository) IsEmailExist(e string) (bool, error) {
	err := r.db.Where("email = ?", e).First(&User{}).Error
	if err != nil {
		if err == gorm.ErrRecordNotFound {
			return false, nil
		}

		return true, err
	}

	return true, nil
}

func (r *userRepository) CreateUser(user *User) (*User, error) {
	err := r.db.Create(user).Error
	if err != nil {
		return nil, err
	}

	return user, nil
}

func (r *userRepository) FindMultiple(args ...interface{}) ([]*User, error) {
	var usr []*User
	v := r.db.Find(usr, args...)
	if v.Error != nil {
		return nil, v.Error
	}

	return usr, nil
}

func (r *userRepository) FindOne(args ...interface{}) (*User, error) {
	usr := &User{}
	v := r.db.Find(usr, args...)
	if v.Error != nil {
		return nil, v.Error
	}

	return usr, nil
}
