package repository

import (
	"context"
	"time"

	uuid "github.com/satori/go.uuid"

	"github.com/jinzhu/gorm"
)

type Note struct {
	Model
	CreatedBy string `gorm:"type:uuid"`
	Title     string
	Text      string
}

func (n *Note) BeforeCreate() error {
	if n.ID == "" {
		n.ID = uuid.NewV4().String()
	}

	return nil
}

type NoteListParams struct {
	IDs           []string
	CreatedBy     []string
	CreatedAtDate []time.Time
	WithDeleted   bool
}

type NoteRepository interface {
	Create(context.Context, *Note) (*Note, error)
	Save(context.Context, *Note) (*Note, error)
	List(context.Context, *NoteListParams) ([]*Note, error)
	Archive(ctx context.Context, id string) error
	Unarchive(ctx context.Context, id string) error
}

type noteRepository struct {
	db *gorm.DB
}

func NewNoteRepository(db *gorm.DB) NoteRepository {
	return &noteRepository{
		db: db,
	}
}

func (n *noteRepository) Create(ctx context.Context, note *Note) (*Note, error) {
	err := n.db.Create(note).Error
	return note, err
}

func (n *noteRepository) Save(ctx context.Context, note *Note) (*Note, error) {
	err := n.db.Save(note).Error
	return note, err
}

func (n *noteRepository) List(ctx context.Context, params *NoteListParams) (res []*Note, err error) {
	d := n.db.New()
	if len(params.IDs) > 0 {
		d = d.Where("id in (?)", params.IDs)
	}

	if len(params.CreatedBy) > 0 {
		d = d.Where("created_by in (?)", params.CreatedBy)
	}

	if len(params.CreatedAtDate) > 0 {
		d = d.Where("date(created_at) in (?)", params.CreatedAtDate)
	}

	err = d.Debug().Find(&res).Error

	return res, err
}

func (n *noteRepository) Archive(ctx context.Context, id string) error {
	return n.db.Delete(&Note{}, "id = ?", id).Error
}

func (n *noteRepository) Unarchive(ctx context.Context, id string) error {
	return n.db.Unscoped().Model(&Note{}).Where("id = ?", id).Update("deleted_at", nil).Error
}
