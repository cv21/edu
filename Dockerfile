FROM golang:latest as builder
COPY . /go/src/bitbucket.org/cv21/edu
RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -ldflags="-w -s" -o /edu /go/src/bitbucket.org/cv21/edu/cmd/edu/main.go

FROM scratch
COPY --from=builder /edu /usr/local/bin/edu
ENTRYPOINT ["/usr/local/bin/edu"]