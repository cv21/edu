package schema

import (
	"bitbucket.org/cv21/edu"
	"bitbucket.org/cv21/edu/repository"
	g "github.com/graphql-go/graphql"
	"github.com/jinzhu/gorm"
	"github.com/satori/go.uuid"
)

var CreateNoteMutation = &g.Field{
	Type: NoteType,
	Args: g.FieldConfigArgument{
		"title": &g.ArgumentConfig{
			Type: g.NewNonNull(g.String),
		},
		"text": &g.ArgumentConfig{
			Type: g.NewNonNull(g.String),
		},
	},
	Resolve: func(p g.ResolveParams) (interface{}, error) {
		noteRp, _ := edu.GetNoteFromCtx(p.Context)
		//currentUser, _ := edu.GetCurrentUserFromCtx(p.Context)

		return noteRp.Create(p.Context, &repository.Note{
			//CreatedBy: currentUser.ID,
			CreatedBy: uuid.NewV4().String(),
			Text:      p.Args["text"].(string),
			Title:     p.Args["title"].(string),
		})
	},
}

var UpdateNoteMutation = &g.Field{
	Type: NoteType,
	Args: g.FieldConfigArgument{
		"id": &g.ArgumentConfig{
			Type: g.NewNonNull(g.ID),
		},
		"title": &g.ArgumentConfig{
			Type: g.NewNonNull(g.String),
		},
		"text": &g.ArgumentConfig{
			Type: g.NewNonNull(g.String),
		},
	},
	Resolve: func(p g.ResolveParams) (interface{}, error) {
		noteRp, _ := edu.GetNoteFromCtx(p.Context)

		notes, err := noteRp.List(p.Context, &repository.NoteListParams{
			IDs: []string{p.Args["id"].(string)},
		})
		if err != nil {
			return nil, err
		}

		if len(notes) == 0 {
			return nil, gorm.ErrRecordNotFound
		}

		note := notes[0]
		note.Text = p.Args["text"].(string)
		note.Title = p.Args["title"].(string)

		return noteRp.Save(p.Context, note)
	},
}

var ArchiveNoteMutation = &g.Field{
	Type: g.Boolean,
	Args: g.FieldConfigArgument{
		"id": &g.ArgumentConfig{
			Type: g.NewNonNull(g.String),
		},
	},
	Resolve: func(p g.ResolveParams) (interface{}, error) {
		noteRp, _ := edu.GetNoteFromCtx(p.Context)
		err := noteRp.Archive(p.Context, p.Args["id"].(string))
		return err == nil, err
	},
}

var UnarchiveNoteMutation = &g.Field{
	Type: g.Boolean,
	Args: g.FieldConfigArgument{
		"id": &g.ArgumentConfig{
			Type: g.NewNonNull(g.String),
		},
	},
	Resolve: func(p g.ResolveParams) (interface{}, error) {
		noteRp, _ := edu.GetNoteFromCtx(p.Context)
		err := noteRp.Unarchive(p.Context, p.Args["id"].(string))
		return err == nil, err
	},
}
