package schema

import (
	"bitbucket.org/cv21/edu"
	"bitbucket.org/cv21/edu/repository"
	"errors"
	g "github.com/graphql-go/graphql"
)

var ListNotes = &g.Field{
	Type: g.NewList(NoteType),
	Args: g.FieldConfigArgument{
		"with_deleted": &g.ArgumentConfig{
			Type: g.Boolean,
		},
	},
	Description: "Returns list of notes",
	Resolve: func(p g.ResolveParams) (interface{}, error) {
		noteRp, err := edu.GetNoteFromCtx(p.Context)
		if err != nil {
			return nil, err
		}

		//currentUser, _ := edu.GetCurrentUserFromCtx(p.Context)

		return noteRp.List(p.Context, &repository.NoteListParams{
			//CreatedBy:   []string{currentUser.ID},
			WithDeleted: p.Args["with_deleted"].(bool),
		})
	},
}

var ReadNote = &g.Field{
	Type: NoteType,
	Args: g.FieldConfigArgument{
		"id": &g.ArgumentConfig{
			Type: g.ID,
		},
	},
	Description: "Returns note",
	Resolve: func(p g.ResolveParams) (interface{}, error) {
		noteRp, err := edu.GetNoteFromCtx(p.Context)
		if err != nil {
			return nil, err
		}

		//currentUser, _ := edu.GetCurrentUserFromCtx(p.Context)

		notes, err := noteRp.List(p.Context, &repository.NoteListParams{
			IDs:   []string{p.Args["id"].(string)},
		})

		if err != nil {
			return nil, err
		}

		if len(notes) == 0 {
			return nil, errors.New("record not found")
		}

		return notes[0], nil
	},
}
