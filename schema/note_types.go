package schema

import (
	"bitbucket.org/cv21/edu"
	"bitbucket.org/cv21/edu/repository"
	"bitbucket.org/cv21/edu/services"
	"bitbucket.org/cv21/edu/util"
	g "github.com/graphql-go/graphql"
)

var (
	NoteType = g.NewObject(g.ObjectConfig{
		Name: "Note",
		Fields: g.Fields{
			"ID": &g.Field{
				Type: g.String,
				// todo seems like a bug in graphql
				Resolve: func(p g.ResolveParams) (interface{}, error) {
					return p.Source.(*repository.Note).ID, nil
				},
			},
			"Text": &g.Field{
				Type: g.String,
			},
			"Title": &g.Field{
				Type: g.String,
			},
			"CreatedAt": &g.Field{
				Type: g.DateTime,
			},
			"CreatedBy": &g.Field{
				Type: UserType,
				Resolve: func(p g.ResolveParams) (interface{}, error) {
					userSvc, err := edu.GetUserFromCtx(p.Context)
					if err != nil {
						return nil, err
					}

					users, err := userSvc.Find(p.Context, services.UserFindParams{
						IDs: []string{p.Source.(*repository.Note).CreatedBy},
					})

					return util.First(users), err
				},
			},
		},
	})

	NoteInput = g.NewObject(g.ObjectConfig{
		Name: "NoteContent",
		Fields: g.Fields{
			"Text": &g.Field{
				Type: g.String,
			},
			"Title": &g.Field{
				Type: g.String,
			},
		},
	})
)
