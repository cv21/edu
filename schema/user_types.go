package schema

import (
	"context"

	"bitbucket.org/cv21/edu"
	"bitbucket.org/cv21/edu/repository"
	"bitbucket.org/cv21/edu/services"
	g "github.com/graphql-go/graphql"
)

func init() {
	UserType.AddFieldConfig("notes", &g.Field{
		Type: g.NewList(NoteType),
		Resolve: func(p g.ResolveParams) (interface{}, error) {
			noteRp, _ := edu.GetNoteFromCtx(p.Context)

			return noteRp.List(p.Context, &repository.NoteListParams{
				CreatedBy: []string{p.Source.(*repository.User).ID},
			})
		},
	})
}

var UserType = g.NewObject(
	g.ObjectConfig{
		Name: "User",
		Fields: g.Fields{
			"email": &g.Field{
				Type: g.String,
				Resolve: func(p g.ResolveParams) (interface{}, error) {
					return p.Source.(*repository.User).Email, nil
				},
			},
			"password": &g.Field{
				Type: g.String,
				Resolve: func(p g.ResolveParams) (interface{}, error) {
					return p.Source.(*repository.User).Password, nil
				},
			},
		},
	},
)

func ResolveUser(ctx context.Context, id string) (*repository.User, error) {
	userSvc, err := edu.GetUserFromCtx(ctx)
	if err != nil {
		return nil, err
	}

	users, err := userSvc.Find(ctx, services.UserFindParams{
		IDs: []string{id},
	})

	if err != nil {
		return nil, err
	}

	if len(users) > 0 {
		return users[0], nil
	}

	return nil, nil
}
