package schema

import g "github.com/graphql-go/graphql"

var Schema = g.SchemaConfig{
	Query: g.NewObject(
		g.ObjectConfig{
			Name: "RootSchema",
			Fields: g.Fields{
				"user":        UserQuery,
				"currentUser": CurrentUserQuery,
				"listNotes":   ListNotes,
				"readNote":    ReadNote,
			},
		},
	),
	Mutation: g.NewObject(
		g.ObjectConfig{
			Name: "RootMutation",
			Fields: g.Fields{
				"login":         LoginMutation,
				"register":      RegisterMutation,
				"createNote":    CreateNoteMutation,
				"archiveNote":   WithAuth(ArchiveNoteMutation),
				"unarchiveNote": WithAuth(UnarchiveNoteMutation),
				"updateNote":    UpdateNoteMutation,
			},
		},
	),
}
