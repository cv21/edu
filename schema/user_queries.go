package schema

import (
	"errors"

	"bitbucket.org/cv21/edu"
	"bitbucket.org/cv21/edu/util"
	g "github.com/graphql-go/graphql"
)

var (
	ErrUnauthorized = errors.New("unauthorized")
)

var UserQuery = &g.Field{
	Type:        UserType,
	Description: "Returns user info",
	Args: g.FieldConfigArgument{
		"id": &g.ArgumentConfig{
			Type: g.NewNonNull(g.String),
		},
	},
	Resolve: func(p g.ResolveParams) (interface{}, error) {
		return ResolveUser(p.Context, p.Args["id"].(string))
	},
}

var CurrentUserQuery = &g.Field{
	Type:        UserType,
	Description: "Returns current user info",
	Resolve: func(p g.ResolveParams) (interface{}, error) {
		currentUser, err := edu.GetCurrentUserFromCtx(p.Context)
		if err != nil || currentUser == nil {
			return nil, util.Err(ErrUnauthorized, err)
		}

		return currentUser, nil
	},
}
