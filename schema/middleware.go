package schema

import (
	"bitbucket.org/cv21/edu"
	"bitbucket.org/cv21/edu/util"
	g "github.com/graphql-go/graphql"
)

func WithAuth(d *g.Field) *g.Field {
	resolveFn := d.Resolve

	d.Resolve = func(p g.ResolveParams) (interface{}, error) {
		currentUser, err := edu.GetCurrentUserFromCtx(p.Context)
		if err != nil || currentUser == nil {
			return nil, util.Err(ErrUnauthorized, err)
		}

		return resolveFn(p)
	}

	return d
}
