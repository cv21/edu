package schema

import (
	"bitbucket.org/cv21/edu"
	g "github.com/graphql-go/graphql"
)

var LoginMutation = &g.Field{
	Type: g.String,
	Args: g.FieldConfigArgument{
		"email": &g.ArgumentConfig{
			Type: g.NewNonNull(g.String),
		},
		"password": &g.ArgumentConfig{
			Type: g.NewNonNull(g.String),
		},
	},
	Resolve: func(p g.ResolveParams) (interface{}, error) {
		email := p.Args["email"].(string)
		password := p.Args["password"].(string)

		userSvc, err := edu.GetUserFromCtx(p.Context)
		if err != nil {
			return nil, err
		}

		return userSvc.Login(p.Context, email, password)
	},
}

var RegisterMutation = &g.Field{
	Type: g.String,
	Args: g.FieldConfigArgument{
		"email": &g.ArgumentConfig{
			Type: g.NewNonNull(g.String),
		},
		"password": &g.ArgumentConfig{
			Type: g.NewNonNull(g.String),
		},
	},
	Resolve: func(p g.ResolveParams) (interface{}, error) {
		email := p.Args["email"].(string)
		password := p.Args["password"].(string)

		userSvc, err := edu.GetUserFromCtx(p.Context)
		if err != nil {
			return nil, err
		}

		_, token, err := userSvc.Register(p.Context, email, password)
		if err != nil {
			return nil, err
		}

		return token, nil
	},
}
